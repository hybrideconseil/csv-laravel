<?php
declare(strict_types=1);

namespace Tests\CSV;

use HybrideConseil\CSV\CSVFactory;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;


class CSVTest extends TestCase
{
    protected $csvFile;

    public function testParse()
    {
        $csv    = new CSVFactory();
        $result = $csv->parse(__DIR__.'/read.csv');

        $this->assertSame('samplename1', $result->lines()[0][0]);
        $this->assertSame('1', $result->lines()[0][1]);
        $this->assertSame('samplename2', $result->lines()[1][0]);
        $this->assertSame('2', $result->lines()[1][1]);
	}

	public function testParseWithHeader()
	{
		$csv = new CSVFactory();
		$result = $csv->parse(__DIR__.'/header.csv', true);

        $this->assertSame(true, $result->hasHeader());
        $this->assertSame('name', $result->header()[0]);
        $this->assertSame('number', $result->header()[1]);

        $this->assertSame('samplename1', $result->lines()[0][0]);
        $this->assertSame('1', $result->lines()[0][1]);
        $this->assertSame('samplename2', $result->lines()[1][0]);
        $this->assertSame('2', $result->lines()[1][1]);
	}

	public function testFromArray()
	{
		$csv = new CSVFactory();
		$arr = array(
			array(
				'name' => 'samplename1',
				'number' => 1
			)
		);
		$result = $csv->create($arr)->build();
		$this->assertSame('samplename1,1'."\n", $result);
	}

	public function testFromArrayWithTabDelimiter()
	{
		$csv = new CSVFactory();
		$arr = array(
			array(
				'name' => 'samplename1',
				'number' => 1
			)
		);
		$result = $csv->create($arr)
						->setDelimiter("\t")
						->build();
		$this->assertSame("samplename1\t1"."\n", $result);
	}

	public function testFromArrayWithHeader()
	{
		$csv = new CSVFactory();
		$arr = array(
			array(
				'name' => 'samplename1',
				'number' => 1
			)
		);
		$header = array(
			'name', 'number'
		);
		$result = $csv->create($arr, $header)->build();
		$this->assertSame('name,number'."\n".'samplename1,1'."\n", $result);
	}

	public function testFromArrayWithBOM_UTF16LE()
	{
		$csv = new CSVFactory();
		$arr = array(
			array(
				'name' => 'テスト',
				'number' => 1
			)
		);
		$result = $csv->setEncode('UTF-16LE', 'UTF-8')
						->create($arr)
						->setBOM_UTF16LE()
						->build();
		var_dump(preg_match('/^\xff\xfe/', $result));
		$this->assertSame(1, preg_match('/^\xff\xfe/', $result));
	}
}