# CSV

CSV manipulation for Laravel 6+.

## Getting started

Add it to composer.json

```sh
'hybrideconseil/csv': '0.4.0'
```

Install it
```
composer install
```

Add ServiceProvider to app.php

```php
'HybrideConseil\CSV\CSVServiceProvider',
```

If you want, add an alias

```php
'CSV' => 'HybrideConseil\CSV\CSVFacade',
```

## Usage

- create from array:

```php
CSV::create($array, $header);
```

- get CSV content:

```php
CSV::create($array, $header)->build();
```

- change encoding:

```php
CSV::setEncode('SJIS-win', 'UTF-8')->create($array, $header)->build();
```

- add BOM:

```php
CSV::create($array, $header)->setBOM_UTF8()->build();
CSV::create($array, $header)->setBOM_UTF16LE()->build();
```

- set delimiter:

```php
CSV::create($array, $header)->setDelimiter("\t")->build();
```

- get CSV content:

```php
CSV::create($array, $header)->render();
```

- read (parse) CSV file:

```php
CSV::parse('sample.csv');
```
