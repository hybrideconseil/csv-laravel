<?php
declare(strict_types=1);

namespace HybrideConseil\CSV;

use Illuminate\Http\Response;
use Illuminate\Http\Testing\FileFactory;
use Illuminate\Support\Facades\Storage;
use SplFileObject;

class CSV
{

    const BOM_UTF8 = "\xEF\xBB\xBF";

    const BOM_UTF16LE = "\xFF\xFE";

    protected $header;

    protected $lines;

    protected $_csv = '';

    protected $delimiter = ',';

    protected $encodeFrom;

    protected $encodeTo;

    protected $bom = '';


    public function __construct($opts)
    {
        $this->header     = $opts['header'] ?? null;
        $this->lines      = $opts['lines'] ?? null;
        $this->encodeFrom = $opts['encodeFrom'] ?? null;
        $this->encodeTo   = $opts['encodeTo'] ?? null;
    }

    /**
     * Begin creating a new file fake.
     *
     * @return \Illuminate\Http\Testing\FileFactory
     */
    public static function fake()
    {
        return new FileFactory;
    }

    public function header()
    {
        return $this->header;
    }

    public function lines()
    {
        return $this->lines;
    }

    public function render($filename = 'download.csv')
    {
        return Response::make($this->build(), 200,
            $this->getResponseHeader($filename));
    }

    public function save($filename = "im_your_father.csv")
    {
        $storageDirectory = __DIR__.'/';
        $data = $this->build();
        Storage::put($storageDirectory.$filename, $data);
        return file_exists($storageDirectory.$filename);
    }

    public function build()
    {
        if ( ! $this->lines) {
            exit();
        }

        $this->tocsv();
        $this->encode();

        return $this->bom.$this->_csv;
    }

    protected function tocsv()
    {
        $sfo = new SplFileObject('php://output', 'r+');
        ob_start();
        if ($this->hasHeader()) {
            $sfo->fputcsv($this->header, $this->delimiter);
        }

        foreach ($this->lines as $l) {
            $sfo->fputcsv($l, $this->delimiter);
        }
        $this->_csv = ob_get_clean();
    }

    public function hasHeader()
    {
        return ! is_null($this->header);
    }

    protected function encode()
    {
        if ( ! $this->_csv) {
            return;
        }
        if ( ! $this->encodeTo) {
            return;
        }

        $this->_csv = mb_convert_encoding($this->_csv, $this->encodeTo,
            $this->encodeFrom);
    }

    protected function getResponseHeader($filename = 'download.csv')
    {
        return [
            'Content-Type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename="'.$filename.'"',
        ];
    }

    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;

        return $this;
    }

    public function setBOM_UTF8()
    {
        $this->bom = self::BOM_UTF8;

        return $this;
    }

    public function setBOM_UTF16LE()
    {
        $this->bom = self::BOM_UTF16LE;

        return $this;
    }

}