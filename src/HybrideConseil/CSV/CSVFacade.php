<?php
declare(strict_types=1);

namespace HybrideConseil\CSV;

use Illuminate\Support\Facades\Facade;

class CSVFacade extends Facade
{

    protected static function getFacadeAccessor()
    {
        return 'csvfacade';
    }

}